require mrfioc2, "2.3.1+9"
require essioc

epicsEnvSet "PREFIX" "LAB-BCM11:Ctrl-EVR-101"

# Load HW settings
iocshLoad "iocrc.iocsh"

# Core
iocshLoad "$(mrfioc2_DIR)/evr.iocsh"      "P=$(PREFIX),PCIID=$(PCIID),EVRDB=$(EVRDB=evr-mtca-300-univ.db)"
dbLoadRecords "evr-databuffer-ess.db"     "P=$(PREFIX)"
iocshLoad "$(mrfioc2_DIR)/evrevt.iocsh"   "P=$(PREFIX),$(EVREVTARGS=)"

iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocInit

iocshLoad "$(mrfioc2_DIR)/evr.r.iocsh" "P=$(PREFIX)"
$(EVRAMC2CLKEN=)iocshLoad "$(mrfioc2_DIR)/evrtclk.r.iocsh" "P=$(PREFIX)"
iocshLoad "$(mrfioc2_DIR)/evrdlygen.r.iocsh" "P=$(PREFIX),$(EVRDLYGENARGS=)"
iocshLoad "$(mrfioc2_DIR)/evrout.r.iocsh" "P=$(PREFIX),$(EVROUTARGS=)"
iocshLoad "$(mrfioc2_DIR)/evrin.r.iocsh" "P=$(PREFIX),$(EVRINARGS=)"

# setup EVR standalone mode
iocshLoad "standalone_mode.iocsh"
